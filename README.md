# Use-Case CO2-Ampel

## Projektbeschreibung

In verschiedenen Innenräumen wurden CO2-Ampeln zur Überwachung des CO2-Gehalts, der Temperatur und der Luftfeuchtigkeit installiert. Diese Geräte zeigen hohe CO2-Werte an und helfen, die Raumtemperatur zu regulieren, um Überhitzung zu vermeiden.

Die CO2-Ampeln senden ihre Messungen 2-minütlich über LoRaWAN-Gateways an ein TTI-Netzwerk. Die Daten werden in die urbane Datenplattform (UDSP) integriert und können über Dashboards visualisiert werden. Dabei werden NGSI-konforme Schnittstellen und Datenmodelle verwendet.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

Die Überwachung des CO2-Gehalts und der Luftfeuchtigkeit in Innenräumen ist aus mehreren Gründen wichtig. Erhöhte CO2-Werte können zu einer schlechten Luftqualität führen, die Schläfrigkeit, Kopfschmerzen und eine Abnahme der kognitiven Funktionen verursachen kann. Richtige Belüftung und die Überwachung des CO2-Gehalts tragen dazu bei, eine gesunde und produktive Umgebung für Menschen zu schaffen. Außerdem ist die Aufrechterhaltung einer optimalen Luftfeuchtigkeit wichtig für Komfort und Gesundheit. Eine hohe Luftfeuchtigkeit kann Schimmelbildung und Unbehagen fördern, während eine niedrige Luftfeuchtigkeit zu trockener Haut, gereizten Atemwegen und einem erhöhten Infektionsrisiko führen kann. Durch die Überwachung des CO2-Gehalts und der Luftfeuchtigkeit ist es möglich, ein komfortableres, gesünderes und effizienteres Raumklima zu schaffen.

## Installationsanleitung

Alle für den Anwendungsfall benötigten Resourcen sind in dem Gitlab.com-Projekt https://gitlab.com/urban-dataspace-platform/use_cases/integration/co2_ampel enthalten. 

Um den Anwendungsfall zu installieren sind folgende Schritte nötig:  
1. Eine lokale Maschine für die Installation ist vorzubereiten (Siehe https://gitlab.com/urban-dataspace-platform/core-platform/-/blob/master/00_documents/Admin-guides/cluster.md).
2. Das [Deployment-Projekt](https://gitlab.com/urban-dataspace-platform/use_cases/integration/co2_ampel/deployment) muss auf die lokale Maschine "geclont" werden (rekursiv!).
3. Die im Deployment beschriebenen Schritte ausführen.

## Gebrauchsanweisung

### CO2-Measurements Flow

Im TTI-Board wird jeder CO<sub>2</sub>-Ampel eine eindeutige ID zugewiesen. Diese eindeutige ID wird im weiteren Verlauf als (eindeutige) "entity-id" verwendet.  
Der Node-RED-Flow abonniert die Sensordaten auf dem Server <i>eu1.cloud.thethings.industries</i>. Da die Qualität der gelieferten Daten sehr unterschiedlich ist, werden die empfangenen Nachrichten zunächst geprüft. Hierbei wird festgestellt, ob:
* es sich um eine "uplink"-Nachricht handelt,
* die Nachricht eine "payload" enthält und
* ob ein "opcode" gesetzt ist und dieser den Wert "Measurements" hat.  

Wenn eine der obigen Bedingungen nicht erfüllt ist, wird die Nachricht abgewiesen und ein entspr. Logeintrag geschrieben. Dieser Logeintrag enthält neben dem Grund für die Abweisung noch die erhaltene Nachricht im Rohformat.  
Erfüllt eine Nachricht alle Anforderungen, werden anschließend aus den Daten NGSI-konforme Nutzdaten erzeugt. Hier wurde das Modell "AirQualityObserved" gewählt. Für eine detaillierte Beschreibung des Datenmodells siehe die entspr. [Dokumentation](https://github.com/smart-data-models/dataModel.Environment/tree/master/AirQualityObserved) auf github.com.  
Beispiel für Nutzdaten:
```json
{
	"id": "co2sek2",
	"type": "AirQualityObserved",
	"dateObserved": {
		"type": "Date",
		"value": "2022-12-08T11:32:35.017656659Z"
	},
	"temperature": {
		"type": "Number",
		"value": 19.4
	},
	"relativeHumidity": {
		"type": "Number",
		"value": 47
	},
	"CO2": {
		"type": "Number",
		"value": 2955
	}
}
```
Diese Nutzdaten werden anschließend an den Orion Context Broker gesendet. Von dort aus werden die Daten mittels "Subscription"-Mechanismus' an QuantumLeap gesendet. Dieser Service wiederum wandelt die NGSI-Daten (JSON) in SQL-konforme Daten und speichert diese in der PostgreSQL-Timeseries Datenbank ab.  
Ein "Catch-All" Knoten fängt alle nicht behandelten Fehler ab und sorgt für einen entsprechenden Eintrag im Log.  

Neben dem eigentlichen Flow für den Datenfluss gibt es noch einen Flow für den Zugriff auf den Orion Context Broker. Mit diesem Flow können die Daten und Subscriptions betrachtet werden.   
Hinweis: Ein mehrfaches Anlegen der Subscription führt zu Datendubletten in den historischen Daten!

### CO2-Grafana Flow

Dieser Flow kann drei Arten von Grafana-Dashboards erstellen.&#x20;

1. Gauge-Dashboard für 1 Schule, um einen aktuellen Überblick zu bekommen.
2. Detailliertes Dashboard für 1 Schule, filterbar nach Sensor. Zeigt Line-Charts und ist somit für historische Betrachtung relevant.
3. Detailliertes Dashboard für mehrere Schulen, filterbar nach Schule. Zeit ebenfalls Line-Charts.

Dashboard 1 und 2 sind z.B. für Schüler:innen, Lehrer:innen und Hausmeister:innen interessant. Dashboard 3 z.B. für ein Gebäudemanagement, das mehrere Schulen beobachtet.

Die Erstellung der Dashboards ist semi-automatisiert. Teilweise müssen die IDs der Sensoren manuell eingepflegt und benannt werden (z.B. Obergeschoss O1). Perspektivisch könnten diese Informationen direkt aus dem TTN gezogen werden. Diese sind dort allerdings noch nicht hinterlegt.

ACHTUNG: Wenn dieser Flow auf einer neuen Plattform eingesetzt wird, müssen in den Template-Knoten, in denen die JSON-Repräsentation der Panels steht, angepasst werden. Die UID der Datenbank wird sich verändern:

```json
"datasource": {
    "uid": "9-QGxoH4k",
    "type": "postgres"
}
```

Außerdem wird im *Create Dashboard payload*-Knoten die UID des Ordners, in den das Dashboard abgelegt werden soll, anders sein (`"folderUid": "_KYlTMBVk"` ). Diese findet man in der URL, wenn man in Grafana einen Ordner aufruft.

#### 1 - Gauge-Dashboard

In dem "Define IDs"-Knoten können Rows mit Sensoren angegeben werden, welche daraufhin automatisiert als Grafana-Dashboard erstellt werden. Die Rows sowie die Sensoren müssen benannt werden, sowie eine ID erhalten. Außerdem kann der Dashboard-Name angegeben werden. Hier wird ein Dashboard "Grundschule" mit einer Row mit 4 Sensoren erzeugt:

```javascript
flow.set("dashboardName", "Grundschule")

msg.payload = [
    {
        "type": "row",
        "title": "Kellergeschoss",
        "panelId": 1,
        "sensors": [
            {
                "title": "K01",
                "sensorId": "co2hoh9",
                "panelId": 2
            },
            {
                "title": "K02",
                "sensorId": "co2hoh10",
                "panelId": 3
            },
            {
                "title": "K12",
                "sensorId": "co2hoh12",
                "panelId": 4
            },
            {
                "title": "K16",
                "sensorId": "co2hoh5",
                "panelId": 5
            }
        ]
    }
]
```

Der Flow erzeugt daraufhin die JSON-Repräsentation des Dashboards, welches an die Grafana-API gesendet wird.

#### 2 - Detaillierte Ansicht eine Schule

Die Konfiguration ist hier einfacher - lediglich in dem *Set variables*-Knoten muss das Kürzel der Schule, mit denen die Sensoren benannt sind sowie der Dashboard-Name eingegeben werden. Wenn die Sensoren z.B. die IDs *co2hoh1, co2hoh2* usw. haben, muss als Schulkürzel `co2hoh` eingegeben werden.&#x20;

Die einzelnen Sensoren selber müssen hier nicht angegeben werden, da die Dashboards sich automatisch die passenden Sensoren aus der Datenbank laden. Dies ist hier möglich, da die Sensoren keinen individuellen Namen bekommen und einer Row zugeordnet werden müssen, anders als bei dem ersten Dashboard.

#### 3 - Detaillierte Ansicht mehrere Schulen

Hier muss in dem Set variables-Knoten die gewünschten Schulen mit dem dazugehörigen Kürzel angegeben werden. Anhand der Kürzel werden mehrere Queries and die PostgreSQL gesendet, um alle benötigten Sensoren zu erhalten. Diese werden dann für das Dashboard zusammengesetzt. Die Konfiguration sieht so aus:

```javascript
msg.dashboardTitle = "Alle Schulen"

msg.payload = [
    {
        "name": "schule1",
        "code": "co2hoh"
    },
    {
        "name": "schule2",
        "code": "co2rho"
    }
]
```

ACHTUNG: Nachdem dieses Dashboard erstellt wurde, muss in Grafana unter *Dashboard settings -> Variables -> Schule -> Update* gedrückt werden. Andernfalls werden die Auswahlmöglichkeiten nicht angezeigt.

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Lizenz

Dieses Projekt wird unter der EUPL 1.2 veröffentlicht.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/co2_ampel/nr-co2-ampel